# Changes in Kiwa Source Collection 0.4

## 0.4.1 2021-04-13

### Fixed

-   Added missing method `getSourceFiles` to get a list of all files that have been found.

## 0.4.0 2021-04-08

### Changed 

-   Each file may lay in a different folder now.
-   The `AbstractSourceCollection` class is abstract now.
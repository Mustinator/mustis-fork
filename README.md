[![PHP from Packagist](https://img.shields.io/packagist/php-v/kiwa/source-collection)](http://www.php.net)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/6acd05e78c024963ad62c829ed59fbc1)](https://www.codacy.com/manual/bitandblack/Kiwa-Core?utm_source=tobiaskoengeter@bitbucket.org&amp;utm_medium=referral&amp;utm_content=wirbelwild/kiwa-core&amp;utm_campaign=Badge_Grade)
[![Latest Stable Version](https://poser.pugx.org/kiwa/source-collection/v/stable)](https://packagist.org/packages/kiwa/source-collection)
[![Total Downloads](https://poser.pugx.org/kiwa/source-collection/downloads)](https://packagist.org/packages/kiwa/source-collection)
[![License](https://poser.pugx.org/kiwa/source-collection/license)](https://packagist.org/packages/kiwa/source-collection)

# Kiwa Source Collection

The Kiwa Source Collection makes it easy to create HTML Audio, Picture and Video elements with multiple sources.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/kiwa/source-collection). Add it to your project by running `$ composer require kiwa/source-collection`. 

## Usage

### Creating a picture collection

To run the library in auto-mode, enable the auto search and tell the root folder, where all the images or video files are stored in:

````php
<?php

use Kiwa\SourceCollection\Picture;

Picture::enableAutoSearchGenerally(
    'path/to/dir'
);
````

After that, create a new Picture object with the file name you want to have to be printed later. This path may be relative.

````php
<?php

$picture = new Picture('/build/images/my-image.jpg');
````

The library will search for files with the same name then and use them as additional sources. Print the object then:

````php
echo $picture; // or $picture->getPicture()
````

Depending on the images there are available, the output could look like that:

````html
<picture>
    <source srcset="/build/images/my-image.webp" type="image/webp"/>
    <source srcset="/build/images/my-image.jpg" type="image/jpeg"/>
    <img src="/build/images/my-image.jpg"/>
</picture>
````

### Creating a video collection

Creating a video element goes in the same way:

````php
<?php

use Kiwa\SourceCollection\Video;

Video::enableAutoSearchGenerally(
    'path/to/dir'
);

echo new Video('/some/path/test-file.mp4');
````

This will result in:

````html
<video>
    <source src="/some/path/test-file.mp4" type="video/mp4"/>
    <source src="/some/path/test-file.mov" type="video/quicktime"/>
</video>
````

### Creating an audio collection

Creating an audio element goes in the same way:

````php
<?php

use Kiwa\SourceCollection\Audio;

Audio::enableAutoSearchGenerally(
    'path/to/dir'
);

echo new Audio('/some/path/test-file.mp3');
````

This will result in:

````html
<audio>
    <source src="/some/path/test-file.mp3" type="audio/mpeg"/>
    <source src="/some/path/test-file.ogg" type="audio/ogg"/>
</audio>
````

### Options 

#### Manual mode

You don't need to use the auto mode. Instead, you can add source files by your own:

````php
<?php

use Kiwa\SourceCollection\Picture;

$picture = new Picture('/build/images/my-image.jpg');
$picture->addSourceFile('/build/images/my-image.webp');
````

#### Attributes 

The constructor and the `addSourceFile` method have a second parameter, which can be used to add HTML parameters:

````php
<?php

use Kiwa\SourceCollection\Picture;

echo new Picture(
    '/build/images/my-image.jpg',
    [
        'alt' => 'This is a alt text',
        'title' => 'This is a title text'
    ]
);
````

This will result in:

````html
<picture>
    <source srcset="/build/images/my-image.jpg" type="image/jpeg"/>
    <img src="/build/images/my-image.jpg" alt="This is a alt text" title="This is a title text"/>
</picture>
````

## Help 

If you have questions about Kiwa feel free to contact us under `hello@kiwa.io`.

<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection\Tests;

use Kiwa\SourceCollection\Picture;
use Kiwa\SourceCollection\Video;
use PHPUnit\Framework\TestCase;

/**
 * Class VideoTest.
 * 
 * @package Kiwa\SourceCollection\Tests
 */
class VideoTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }
    
    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }

    /**
     * @runInSeparateProcess
     */
    public function testCanHandleVideoAutomatically(): void
    {
        Video::enableAutoSearchGenerally(
            __DIR__
        );

        $video = Video::create(
            '/some/path/test-file.mp4',
            [
                'width' => 250,
                'height' => 100,
                'muted' => true,
            ]
        );

        self::assertSame(
            '<video width="250" height="100" muted="muted">
  <source src="/some/path/test-file.mp4" type="video/mp4"/>
  <source src="/some/path/test-file.mov" type="video/quicktime"/>
</video>',
            $video->getVideo()
        );
    }
    /**
     * @runInSeparateProcess
     */
    public function testCanHandleVideoManually(): void
    {
        $video = Video::create(
                '/some/path/test-file.mp4',
                [
                    'width' => 250,
                    'height' => 100,
                    'muted' => true,
                ]
            )
            ->addSourceFile('/some/other/path/test-file.mp4')
            ->addSourceFile('/some/path/test-file.mov')
        ;

        self::assertSame(
            '<video width="250" height="100" muted="muted">
  <source src="/some/other/path/test-file.mp4" type="video/mp4"/>
  <source src="/some/path/test-file.mp4" type="video/mp4"/>
  <source src="/some/path/test-file.mov" type="video/quicktime"/>
</video>',
            $video->getVideo()
        );
    }
    public function testCanCreateDom(): void
    {
        $video = new Video(
            '/some/path/test-file.mp4',
            [
                'alt' => 'This is a description',
                'title' => 'This is a description'
            ]
        );

        $xml = $video->getVideoDOMDocument();

        self::assertCount(
            1,
            $xml->getElementsByTagName('video')
        );

        self::assertCount(
            1,
            $xml->getElementsByTagName('source')
        );

        self::assertEquals(
            'This is a description',
            $xml->getElementsByTagName('video')[0]->getAttribute('alt')
        );
    }
}

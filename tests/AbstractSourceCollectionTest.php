<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection\Tests;

use Kiwa\SourceCollection\AbstractSourceCollection;
use Kiwa\SourceCollection\Audio;
use Kiwa\SourceCollection\Picture;
use Kiwa\SourceCollection\Video;
use PHPUnit\Framework\TestCase;

/**
 * Class AbstractSourceCollectionTest. 
 * 
 * @package Kiwa\SourceCollection\Tests
 */
class AbstractSourceCollectionTest extends TestCase
{
    public function testCanSetDifferentFolders(): void 
    {
        AbstractSourceCollection::enableAutoSearchGenerally(__DIR__);
        
        Audio::enableAutoSearchGenerally(__DIR__.DIRECTORY_SEPARATOR.'test-folder-audio');
        $audio = new Audio('./audio.mp3');

        Picture::enableAutoSearchGenerally(__DIR__.DIRECTORY_SEPARATOR.'test-folder-picture');
        $picture = new Picture('./picture.jpg');

        Video::enableAutoSearchGenerally(__DIR__.DIRECTORY_SEPARATOR.'test-folder-video');
        $video = new Video('./video.mp4');

        self::assertStringContainsString(
            'audio.ogg',
            (string) $audio
        );
        
        self::assertStringContainsString(
            'audio.mp3',
            (string) $audio
        );

        self::assertStringContainsString(
            'picture.jpg',
            (string) $picture
        );

        self::assertStringContainsString(
            'picture.webp',
            (string) $picture
        );

        self::assertStringContainsString(
            'video.webm',
            (string) $video
        );

        self::assertStringContainsString(
            'video.mp4',
            (string) $video
        );
        
        self::assertCount(
            2,
            $audio->getSourceFiles()
        );
    }
}

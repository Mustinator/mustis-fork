<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection\Tests;

use Kiwa\SourceCollection\Picture;
use PHPUnit\Framework\TestCase;

/**
 * Class PictureTest.
 * 
 * @package App\Tests
 */
class PictureTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        Helper::setUpBefore();
    }

    public static function tearDownAfterClass(): void
    {
        Helper::tearDownAfter();
    }
    
    /**
     * @runInSeparateProcess
     */
    public function testCanHandleImageAutomatically(): void
    {
        Picture::enableAutoSearchGenerally(
            __DIR__
        );

        $picture = new Picture(
            '/some/path/test-file.jpg',
            [
                'alt' => 'This is a description',
                'title' => 'This is a description'
            ]
        );

        self::assertSame(
            '<picture>
  <source srcset="/some/path/test-file.webp" type="image/webp"/>
  <source srcset="/some/path/test-file.jpg" type="image/jpeg"/>
  <source srcset="/some/path/test-file.png" type="image/png"/>
  <img src="/some/path/test-file.jpg" alt="This is a description" title="This is a description"/>
</picture>',
            $picture->getPicture()
        );
    }
    
    /**
     * @runInSeparateProcess
     */
    public function testCanHandleImageManually(): void
    {
        $picture = Picture::create(
            '/some/path/test-file.jpg',
                [
                    'alt' => 'This is a description',
                    'title' => 'This is a description'
                ]
            )
            ->addSourceFile('/some/path/test-file.webp', ['media' => '(min-width: 600px)'])
        ;

        self::assertSame(
            '<picture>
  <source srcset="/some/path/test-file.webp" type="image/webp" media="(min-width: 600px)"/>
  <source srcset="/some/path/test-file.jpg" type="image/jpeg"/>
  <img src="/some/path/test-file.jpg" alt="This is a description" title="This is a description"/>
</picture>',
            $picture->getPicture()
        );
    }
    
    public function testCanCreateDom(): void 
    {
        $picture = new Picture(
            '/some/path/test-file.jpg',
            [
                'alt' => 'This is a description',
                'title' => 'This is a description'
            ]
        );
        
        $xml = $picture->getPictureDOMDocument();

        self::assertCount(
            1,
            $xml->getElementsByTagName('picture')
        );
        
        self::assertCount(
            1,
            $xml->getElementsByTagName('source')
        );

        self::assertCount(
            1,
            $xml->getElementsByTagName('img')
        );

        self::assertEquals(
            'This is a description',
            $xml->getElementsByTagName('img')[0]->getAttribute('alt')
        );
    }
}

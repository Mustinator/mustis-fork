# Changes in Kiwa Source Collection 0.3

## 0.3.0 2020-12-18

### Added 

-   Added possibility to create `audio` collections with the new class `Audio`.
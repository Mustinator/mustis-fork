# Changes in Kiwa Source Collection 0.1

## 0.1.1 2020-11-25

### Changed

-   Added support for PHP 8.

## 0.1.0 2020-11-11

### Added 

-   Added possibility to add multiple folders on `enableAutoSearchGenerally()` and `enableAutoSearch()`.
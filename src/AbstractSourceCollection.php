<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection;

use DOMDocument;
use Symfony\Component\Mime\MimeTypes;

/**
 * Class AbstractSourceCollection.
 * 
 * @package App
 */
abstract class AbstractSourceCollection
{
    /**
     * @var bool|null
     */
    protected static $autoSearchEnabledGlobally;

    /**
     * @var bool|null
     */
    private $autoSearchEnabled;

    /**
     * @var \DOMDocument
     */
    private $domDocument;

    /**
     * These are the next-gen formats, that need to be displayed at first.
     * A browser will take the first possibility, and those should be the newest formats.
     *
     * @var array<int, string>
     */
    protected $nextGenFormats = [];

    /**
     * @var array<int, string>|null
     */
    protected static $defaultRootFolderGlobally;

    /**
     * @var array<int, string>|null
     */
    private $rootFolder;

    /**
     * @var string
     */
    protected $rootFileSrc;

    /**
     * @var array<string, mixed>
     */
    protected $rootFileOptions;

    /**
     * @var array<string, array<string, string>>
     */
    private $sourceFiles = [];
    
    /**
     * @var array<string, mixed>
     */
    private $attributes = [];
    
    /**
     * @var bool
     */
    private $isPicture;
    
    /**
     * @var bool
     */
    private $isVideo;
    
    /**
     * @var bool
     */
    private $isAudio;

    /**
     * AbstractSourceCollection constructor.
     */
    public function __construct()
    {
        $this->domDocument = new DOMDocument();
        $this->domDocument->formatOutput = true;
        $this->isAudio = $this instanceof Audio;
        $this->isPicture = $this instanceof Picture;
        $this->isVideo = $this instanceof Video;
    }

    /**
     * Returns the collection as XML.
     *
     * @param string $nodeName
     * @return \DOMDocument
     */
    protected function getCollectionDOMDocument(string $nodeName): DOMDocument
    {
        $collection = $this->domDocument->createElement($nodeName);

        foreach ($this->attributes as $attributeName => $attributeValue) {
            if (true === $attributeValue) {
                $attributeValue = $attributeName;
            }

            $collection->setAttribute($attributeName, $attributeValue);
        }

        $dirName = pathinfo($this->rootFileSrc, PATHINFO_DIRNAME);
        $fileName = pathinfo($this->rootFileSrc, PATHINFO_FILENAME);

        if ((true === self::$autoSearchEnabledGlobally && false !== $this->autoSearchEnabled)
            || (false !== self::$autoSearchEnabledGlobally && true === $this->autoSearchEnabled)
        ) {
            $folders = $this->rootFolder ?? $this->getAutoSearchFolders() ?? self::$defaultRootFolderGlobally ?? [];
            $filesSameNamed = [];

            foreach ($folders as $folder) {
                $files = glob($folder.DIRECTORY_SEPARATOR.$fileName.'.*');

                if (!is_array($files)) {
                    continue;
                }

                array_push($filesSameNamed, ...$files);
            }

            foreach ($filesSameNamed as $fileSameNamed) {
                $fileType = $this->getFileType($fileSameNamed);
                $fileTypes = explode('/', $fileType);

                $fileNameRelative = $dirName.DIRECTORY_SEPARATOR.pathinfo($fileSameNamed, PATHINFO_BASENAME);

                if ($this->isAudio && $fileTypes[0] === 'audio') {
                    $this->sourceFiles[$fileNameRelative] = [];
                } elseif ($this->isPicture && $fileTypes[0] === 'image') {
                    $this->sourceFiles[$fileNameRelative] = [];
                } elseif ($this->isVideo && $fileTypes[0] === 'video') {
                    $this->sourceFiles[$fileNameRelative] = [];
                } 
            }
        }

        $this->sourceFiles[$this->rootFileSrc] = [];

        $this->sourceFiles = $this->sortNextGenFormatAtFirst($this->sourceFiles);

        $sourceAttributeName = $this->isPicture ? 'srcset' : 'src';

        foreach ($this->sourceFiles as $src => $options) {
            $source = $this->domDocument->createElement('source');
            $source->setAttribute($sourceAttributeName, $src);
            $source->setAttribute('type', $this->getFileType($src));

            foreach ($options as $optionName => $optionValue) {
                $source->setAttribute($optionName, $optionValue);
            }

            $collection->appendChild($source);
        }

        if ($this->isPicture) {
            $img = $this->domDocument->createElement('img');
            $img->setAttribute('src', $this->rootFileSrc);

            foreach ($this->rootFileOptions as $attributeName => $attributeValue) {
                $img->setAttribute($attributeName, $attributeValue);
            }

            $collection->appendChild($img);
        }

        $this->domDocument->appendChild($collection);

        return $this->domDocument;
    }

    /**
     * Renders and returns the collection to HTML.
     *
     * @param string $nodeName
     * @return string
     */
    protected function getCollection(string $nodeName): string
    {
        return (string) $this
            ->getCollectionDOMDocument($nodeName)
            ->saveXML(
                $this->domDocument->documentElement
            )
        ;
    }

    /**
     * Enables that related files are automatically searched and added.
     *
     * @param string ...$rootFolder One or multiple paths to the folder(s) where the files are stored in.
     * @return \Kiwa\SourceCollection\AbstractSourceCollection
     */
    public function enableAutoSearch(string ...$rootFolder): self 
    {
        $this->autoSearchEnabled = true;
        $this->rootFolder = $rootFolder;
        return $this;
    }

    /**
     * Enables that related files are automatically searched and added.
     * This method takes place as a global setting.
     *
     * @param string ...$defaultRootFolder One or multiple paths to the folder(s) where the files are stored in.
     */
    public static function enableAutoSearchGenerally(string ...$defaultRootFolder): void
    {
        self::$autoSearchEnabledGlobally = true;
        self::$defaultRootFolderGlobally = $defaultRootFolder;
    }

    /**
     * Returns the auto search folders, that have been set for a specific media type. 
     * 
     * @return array<int, string>|null
     */
    abstract protected function getAutoSearchFolders(): ?array;

    /**
     * Disables that related files are automatically searched and added. This behaviour is disabled per default.
     * This method takes place as a global setting.
     */
    public static function disableAutoSearchGenerally(): void
    {
        self::$autoSearchEnabledGlobally = false;
    }

    /**
     * Disables that related files are automatically searched and added. This behaviour is disabled per default.
     */
    public function disableAutoSearch(): self
    {
        $this->autoSearchEnabled = false;
        return $this;
    }

    /**
     * Manually adds a source file.
     *
     * @param string $fileSrc                    The absolute or relative URL of the file.
     *                                           This path will be displayed when creating the HTML code.
     * @param array<string, string> $fileOptions All attributes for the file, like `media`, or `sizes`.
     *                                           The `type` and `src` attributes will be added automatically.
     * @return $this
     */
    public function addSourceFile(string $fileSrc, array $fileOptions = []): self
    {
        $this->sourceFiles[$fileSrc] = $fileOptions;
        return $this;
    }

    /**
     * Returns all files that have been found.
     * 
     * @return array<string, array<string, string>>
     */
    public function getSourceFiles(): array
    {
        return $this->sourceFiles;
    }

    /**
     * Returns the file type based on its name.
     *
     * @param string $fileSrc
     * @return string
     */
    private function getFileType(string $fileSrc): string
    {
        $extension = pathinfo($fileSrc, PATHINFO_EXTENSION);
        $mimeTypes = MimeTypes::getDefault()->getMimeTypes($extension);
        return $mimeTypes[0] ?? 'application/octet-stream';
    }

    /**
     * Sets attributes to the root element.
     * 
     * @param array<string, mixed> $attributes
     * @return $this
     */
    public function addAttributes(array $attributes): self  
    {
        $this->attributes = array_merge($this->attributes, $attributes);
        return $this;
    }

    /**
     * @param array<string, array<string, mixed>> $sources
     * @return array<string, array<string, mixed>>
     */
    private function sortNextGenFormatAtFirst(array $sources): array 
    {
        uksort(
            $sources, 
            function ($itemA, $itemB): int 
            {
                $extensionA = pathinfo($itemA, PATHINFO_EXTENSION);
                $extensionB = pathinfo($itemB, PATHINFO_EXTENSION);
    
                $matchA = array_search($extensionA, $this->nextGenFormats, true);
                $matchB = array_search($extensionB, $this->nextGenFormats, true);
    
                if (false === $matchA && false === $matchB) {
                    return 0;
                }
    
                if (false === $matchA) {
                    return 1;
                }
    
                if (false === $matchB) {
                    return -1;
                }
    
                return $matchA - $matchB;
            }
        );
        
        return $sources;
    }
}
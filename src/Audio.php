<?php

/**
 * Kiwa Source Collection. Handling HTML Audio, Picture and Video elements.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © 2021 Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\SourceCollection;

use DOMDocument;

/**
 * The Audio class creates a Audio HTML element. For example:
 * 
 * ```
 * <audio controls="controls">
 *     <source src="/my-audio.mp3" type="audio/mpeg">
 * </audio>
 * ```
 * 
 * @package BitAndBlack
 */
class Audio extends AbstractSourceCollection
{
    /**
     * @var array<int, string>|null
     */
    protected static $defaultRootFolderGlobally;

    /**
     * These are the next-gen formats, that need to be displayed at first.
     * A browser will take the first possibility, and those should be the newest formats.
     *
     * @var array<int, string>
     */
    protected $nextGenFormats = [
        'mp3',
        'ogg',
        'wav',
    ];

    /**
     * Creates a new audio object.
     *
     * @param string $audioSrc                   The absolute or relative URL of the audio.
     *                                           This path will be displayed when creating the HTML code.
     * @param array<string, mixed> $audioOptions All attributes for the audio tag, like `mute`, `loop`, or `controls`.
     */
    final public function __construct(string $audioSrc, array $audioOptions = [])
    {
        $this->rootFileSrc = $audioSrc;
        $this->addAttributes($audioOptions);
        parent::__construct();
    }

    /**
     * Creates a new audio object.
     *
     * @param string $audioSrc                   The absolute or relative URL of the audio.
     *                                           This path will be displayed when creating the HTML code.
     * @param array<string, mixed> $audioOptions All attributes for the audio tag, like `mute`, `loop`, or `controls`.
     * @return \Kiwa\SourceCollection\Audio
     */
    public static function create(string $audioSrc, array $audioOptions = []): self
    {
        return new static($audioSrc, $audioOptions);
    }
    
    /**
     * Returns the audio as HTML.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getAudio();
    }
    
    /**
     * Returns the audio as XML.
     *
     * @return \DOMDocument
     */
    public function getAudioDOMDocument(): DOMDocument
    {
        return $this->getCollectionDOMDocument('audio');
    }

    /**
     * Renders and returns the audio as HTML.
     *
     * @return string
     */
    public function getAudio(): string
    {
        return $this->getCollection('audio');
    }

    /**
     * Enables that related files are automatically searched and added.
     * This method takes place as a global setting.
     *
     * @param string ...$defaultRootFolder One or multiple paths to the folder(s) where the files are stored in.
     */
    public static function enableAutoSearchGenerally(string ...$defaultRootFolder): void
    {
        self::$autoSearchEnabledGlobally = true;
        self::$defaultRootFolderGlobally = $defaultRootFolder;
    }

    /**
     * Returns the auto search folders, that have been set for a specific media type.
     * 
     * @return array<int, string>|null
     */
    protected function getAutoSearchFolders(): ?array
    {
        return self::$defaultRootFolderGlobally;
    }
}